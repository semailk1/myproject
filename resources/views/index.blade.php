<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<body>

<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
    <symbol id="check" viewBox="0 0 16 16">
        <title>Check</title>
        <path
            d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"></path>
    </symbol>
</svg>

<div class="container py-3">
    <header>
        <div class="d-flex flex-column flex-md-row align-items-center pb-3 mb-4 border-bottom">
            <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" class="me-2" viewBox="0 0 118 94"
                     role="img"><title>Bootstrap</title>
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z"
                          fill="currentColor"></path>
                </svg>
                <span class="fs-4">Pricing example</span>
            </a>

            <nav class="d-inline-flex mt-2 mt-md-0 ms-md-auto">
                <a class="me-3 py-2 text-dark text-decoration-none" href="#">Features</a>
                <a class="me-3 py-2 text-dark text-decoration-none" href="#">Enterprise</a>
                <a class="me-3 py-2 text-dark text-decoration-none" href="#">Support</a>
                <a class="py-2 text-dark text-decoration-none" href="#">Pricing</a>
            </nav>
        </div>
    </header>


    <form method="post" action="{{ route('store') }}">
        @csrf
        <div class="col-md-12">
            <label for="first-name">Имя</label>
            <input id="first-name" class="form-control" name="first_name" required type="text">
        </div>

        <div class="col-md-12">
            <label for="last-name">Фамилия</label>
            <input id="last-name" class="form-control" name="last_name" required type="text">
        </div>

        <div class="col-md-12">
            <label for="surname-name">Отчество</label>
            <input id="surname-name" class="form-control" name="surname" required type="text">
        </div>
        <div class="col-md-12">
            <label for="dont-surname">Нету отчества</label>
            <input id="dont-surname" name="dont_surname" type="checkbox">
        </div>
        <div class="col-md-4">
            <label for="date_of_birth">Дата рождения</label>
            <input type="date" id="date_of_birth" name="date_of_birth" class="form-control">
        </div>

        <button class="btn btn-primary mt-2">Записаться</button>
    </form>

    @if(session('success'))
        <div class="alert-success">
            {{ session('success') }}
        </div>
        @endif

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert-danger">{{ $error }}</div>
        @endforeach
    @endif
</div>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('#dont-surname').change(function () {
            if ($(this).prop('checked')) {
                $('#surname-name').prop("disabled", true);
            } else {
                $('#surname-name').prop("disabled", false);
            }
        })
    })
</script>

</body>
</body>
</html>
