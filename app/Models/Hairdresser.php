<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $first_name
 * @property string $last_name
 * @property string $surname
 * @property string $date_of_birth
 * @property string $session
 */
class Hairdresser extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'surname',
        'date_of_birth',
        'session'
    ];
}
