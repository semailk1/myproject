<?php

namespace App\Http\Requests;

use App\Rules\PregMatch;
use Illuminate\Foundation\Http\FormRequest;

class HairdresserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => [
                'required',
                'string',
                new PregMatch()
            ],
            'last_name' => [
                'required',
                'string',
                new PregMatch()
            ],
            'surname' => [
                'string'
            ],
            'date_of_birth' => [
                'required',
            ]
        ];
    }

    public function messages()
    {
        return [
          'first_name.required' => 'Имя - обязательное поле к заполнению',
          'last_name.required' => 'Фамилия - обязательна к заполнению',
            'date_of_birth.required' => 'Дата - поле обязательна к заполнению'
        ];
    }
}
