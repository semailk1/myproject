<?php

namespace App\Http\Controllers;

use App\Http\Requests\HairdresserRequest;
use App\Models\Hairdresser;
use App\Service\HairdresserService;
use Illuminate\View\View;

class HairdresserController extends Controller
{
    public function __construct(public HairdresserService $hairdresserService)
    {
        //
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('index');
    }

    public function store(HairdresserRequest $request)
    {
        $hairdresser = new Hairdresser();
        $this->hairdresserService->pregMatch($hairdresser, $request);
        $hairdresser->save();

        return redirect()->back()->with(['success' => 'Успешно сохранено!']);
    }

}
