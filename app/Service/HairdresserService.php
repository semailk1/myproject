<?php

namespace App\Service;

use App\Http\Requests\HairdresserRequest;
use Carbon\Carbon;

class HairdresserService
{
    public function pregMatch($hairdresser, HairdresserRequest $request)
    {
        $hairdresser->first_name = $request->first_name;
        $hairdresser->last_name = $request->last_name;
        if ($request->has('dont_surname')) {
            $hairdresser->surname = null;
        } else {
            $hairdresser->surname = $request->surname;
        }
        $hairdresser->session = Carbon::now();
        $hairdresser->date_of_birth = $request->date_of_birth;

        return $hairdresser;
    }
}
